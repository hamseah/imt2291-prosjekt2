<?php

/**
 *  This script lets a teacher delete his own video
 *  If the operation (POST- variable) is set to "delete" the video gets deleted
 *  And if its set to edit the else - block is executed for editing
 *  This edits/ deletes the video from all the entries (3) tables, videoTag, tag and video
 **/

require_once 'config.php';
require_once '../Classes/DB.php';

session_start();

header("Access-Control-Allow-Origin: ".$config['AccessControlAllowOrigin']);
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");


$db = DB::getDBConnection();
$tmp = [];
// Deletes one video from all entries
if($_POST['operation'] == 'delete'){
    try {
        $path = '../videos/'.$_POST['id'].'.mp4';
        //Getting the correct tagname first
        $sql = "SELECT tagName from videotag where videoId= ?";
        $sth = $db->prepare($sql);
        $sth->execute(array($_POST['id']));
        $tag = $sth->fetch(PDO::FETCH_ASSOC);

        // Deleting first from videotag then tag and finally video.
        $sql = "DELETE FROM videotag where videoId= ?";
        $sth = $db->prepare($sql);
        $sth->execute(array($_POST['id']));

        $sql = "DELETE FROM tag where name= ?";
        $quiry = $db->prepare($sql);
        $quiry->execute(array($tag['tagName']));
        
        $sql = "DELETE FROM video where id= ?";
        $th = $db->prepare($sql);
        $th->execute(array($_POST['id']));

        // Checking if everything went OK
        if ($sth->rowCount() == 1 && $th->rowCount() == 1 && $quiry->rowCount() == 1) {
            //Now we delete the video from the folder.
            if(!unlink($path)){
                $tmp['status'] = 'FAIL';
                $tmp['info'] = 'Video was not deleted from folder';
            } else {
                $tmp['status'] = 'OK';
            }
            
        } else {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to delete the video';
            $tmp['error_first'] = $sth->errorInfo();
            $tmp['error_sec'] = $quiry->errorInfo();
            $tmp['error_third'] = $th->errorInfo();
        }
        echo json_encode($tmp);
    } catch (Exception $e) {
        // Error messages for debuging
        $tmp['status'] = 'FAIL';
        $tmp['errorMessage'] = 'failed to delete video';
        $tmp['errorInfo'] = $sth->errorInfo();
        echo json_encode($tmp);
    }
} else {
    //Editing the choosen video
    try {
        
        $sql = "UPDATE video set title= ?, description= ? where id= ?";
        $sth = $db->prepare($sql);
        $sth->execute(array($_POST['title'], $_POST['description'], $_POST['id']));

        // Checking if everything went OK
        if ($sth->rowCount() == 1) {
            $tmp['feedback'] = 'OK';
        } else {
            $tmp['feedback'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to delete the video';
            $tmp['error_first'] = $sth->errorInfo();
            $tmp['error_sec'] = $quiry->errorInfo();
            $tmp['error_third'] = $th->errorInfo();
        }

        echo json_encode($tmp);
    } catch (Exception $e) {
        // Error messages for debuging
        $tmp['feedback'] = 'FAIL';
        $tmp['errorMessage'] = 'failed to delete video';
        $tmp['errorInfo'] = $sth->errorInfo();
        echo json_encode($tmp);
    }

}