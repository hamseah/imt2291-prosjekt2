<?php
/**
 * This script is used for logging in/out and checking for logged in status.
 * If called with method POST it is used to log users in to the system.
 * If called with method GET and the variable 'logoug' set it logs the user out.
 * If none of the above the $_SESSION variable is used to determine is a users
 * is logged in or not.
 */

require_once 'config.php';
require_once '../Classes/user.php';

session_start();

header("Access-Control-Allow-Origin: ".$config['AccessControlAllowOrigin']);
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");

//Initiating the object here, because im going to use it in multiple scope
$user = new User();

if (isset($_POST['email']) && isset($_POST['password'])){
    $user = new User();
    $res = $user->login(array(
    'email' => $_POST['email'], 'password' => $_POST['password']
    ));
    if($res['status'] == 'OK'){
      //Email and password was correct, granting user access and updating session
      $_SESSION['user'] = $_POST['email'];
      $_SESSION['fname'] = $res['fname'];
      $_SESSION['lname'] = $res['lname'];
      $_SESSION['accessLevel'] = $res['role'];
      echo json_encode($res);
    } else {
      echo json_encode($res);
    }
} else if (isset($_GET['logout'])){
  //unsetting sessions when user logs out
    unset($_SESSION['user']);
    unset($_SESSION['fname']);
    unset($_SESSION['lname']);
    unset($_SESSION['accessLevel']);
    echo json_encode(array(
      'status' => 'logged_out', 'role' => 0
    ));
} else if (isset($_SESSION['user'])) {
  //The user is logged in via session
  $accessLvl = $user->getAccessLevel($_SESSION['user']);
  echo json_encode(array(
    'status' => 'OK', 'role' => $accessLvl, 'fname' => $_SESSION['fname'], 'lname' => $_SESSION['lname']
  ));
} else {
  //For debuging when loging fails
  echo json_encode(array(
    'status' => 'FAIL', 'errorInfo' => 'missing email or password!',
    'role' => 0 
  ));
}




