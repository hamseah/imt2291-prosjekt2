<?php

/**
*  Skal være mellomledd mellom klienten og db-en. Får inn data fra brukeren i form av POST
*  Legger inn/ oppdaterer i db.
*  Deretter sender info tilbake, om status
**/

require_once 'config.php';
require_once '../Classes/DB.php';
require_once '../Classes/video.php';
require_once '../Classes/user.php';

session_start();

header("Access-Control-Allow-Origin: ".$config['AccessControlAllowOrigin']);
//header("Access-Control-Allow-Origin:*");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");

$db = DB::getDBConnection();
$user = new User();
$accessLevel = 0;
$data = [];

//checking if the user is logged in.
if(isset($_SESSION['user'])){
  $accessLevel = $user->getAccessLevel($_SESSION['user']);

  //Checking if there is no error with the upload and user have teacher/ admin accesslevel
  if($_FILES['file']['error'] === 0 && $accessLevel > 1){

    //All the needed info about the video
    $data['tmp_n']= $_FILES['file']['name'];
    $data['tmp_dest'] = $_FILES['file']['tmp_name'];
    $data['error'] = $_FILES['file']['error'];
    $type = $_FILES['file']['type'];

    $data['title'] = $_POST['title'];
    $data['tag'] = $_POST['tag'];
    $data['description'] = $_POST['description'];
    $temp = explode('/', $type);
    $data['type'] = $temp[1];


    $data['destination'] = '../videos/'.$data['tmp_n'];
    //Checks if the video folder exists, otherwise it is created
    if(isset($_SESSION['user'])){
      if (!file_exists('../videos/')) {
        $data['feedback'] = "Video directory does not exist, creating new...";
        @mkdir('../videos/');
      }
      $data['email'] = $_SESSION['user'];
    } else {

    }

    try {
      //Check if tag exists
      $sql = 'SELECT name
      FROM tag
      WHERE name = :tag';
      //Go through every tag
      $sth = $db->prepare($sql);
      $sth->bindValue(':tag', $data['tag'], PDO::PARAM_STR);
      $sth->execute();
      // If tag dosent exists, create new
      if ($sth->rowCount() != 1) {
        $sql = 'INSERT INTO Tag(name)
        VALUES(:name)';
        $sth = $db->prepare($sql);
        $sth->bindValue(':name', $data['tag'], PDO::PARAM_STR);
        $sth->execute();

      } else {
        die(json_encode(array('status' => 'FAIL', 'errorInfo' => 'tag already excist', 'totalInfo' => $data)));
      }

    } catch (Exception $e) {
      die(json_encode(array('status' => 'FAIL', 'errorInfo' => $e->getMessage(), 'totalInfo' => $data)));
    }

    // FOR UPLOAAADING AV VIDEO TIL MAPPE!!
    //move_uploaded_file($data['tmp_dest'], $data['destination']);
    try { 
      // Uploading the video into the db
      $sql = "INSERT INTO Video(title, description, author) VALUES (:title, :description, :author)";
      $sth = $db->prepare($sql);

      $sth->bindValue(':title', $data['title'], PDO::PARAM_STR);
      $sth->bindParam(':description', $data['description'], PDO::PARAM_STR);
      $sth->bindParam(':author', $_SESSION['user'], PDO::PARAM_STR);

      $sth->execute();
      $id = $db->lastInsertId();
      $name = $id.'.'.$data['type'];

      //Updating videoTag table in db too
      $sql = 'INSERT INTO videoTag(videoId, tagName) VALUES (?, ?)';
      $sth = $db->prepare($sql);
      $sth->execute(array(
        $id, $data['tag']
      ));

      //Checking if everything worked
      if ($sth->rowCount() == 1) {
        if(move_uploaded_file($data['tmp_dest'], '../videos/'.$name)) 
          $data['feedback'] = 'Vid moved to right folder';
        else // Stops the scripts and tells the user what happened.
          die(json_encode(array('status' => 'FAIL', 'errorInfo' => 'failed to move vid to folder', 'totalInfo' => $data)));
          
      } else { // Stops the scripts and tells the user what happened.
        die(json_encode(array('status' => 'FAIL', 'errorInfo' => 'failed to insert vid in db', 'totalInfo' => $data)));
      }

    } catch (Exception $e){
      $data['error'] = $e->getMessage();
      $data['errorInfo'] = $sth->errorInfo();
      echo json_encode(array('status' => 'FAIL', 'errorInfo' => $data['errorInfo'], 'totalInfo' => $data));
    }

    echo json_encode(array('status' => 'OK', 'totalInfo' => $data));
    
  } else {
    echo json_encode(array('status' => 'FAIL', 'errorInfo' => '(file-error was not zero), errornr: '.$_FILES['file']['error']));
  }

} else {
  echo json_encode(array('status' => 'FAIL', 'errorInfo' => 'accesslevel too low: '.$accessLevel));
}


