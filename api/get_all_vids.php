<?php

/**
 *  Gets all the vids so any user can watch them
 *  And the metadata for the videos, such as title, author and so on
 **/
require_once 'config.php';
require_once '../Classes/DB.php';

session_start();

header("Access-Control-Allow-Origin: ".$config['AccessControlAllowOrigin']);
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");


$db = DB::getDBConnection();
$tmp = [];
// Retrives all the videos
if(isset($_GET['getVids'])){
    try {
        $sql = "SELECT * FROM video";
        $sth = $db->prepare($sql);
        $sth->execute();
        $tmp['all_vids'] = $sth->fetchAll(PDO::FETCH_ASSOC);
        
        //Getting all tags for the vids
        //Getting the authors name
        $data['data'] = [];
        foreach($tmp['all_vids'] as $author){
            $sql = "SELECT * FROM user where email= ?";
            $sth = $db->prepare($sql);
            $sth->execute(array($author['author']));
            $my_author= $sth->fetch(PDO::FETCH_ASSOC);

            $sql = "SELECT * FROM videotag where videoId= ?";
            $sth = $db->prepare($sql);
            $sth->execute(array($author['id']));
            $tag = $sth->fetch(PDO::FETCH_ASSOC);
            
            // putting it all together so i can use dome- repeat
            array_push($data['data'], array(
                'id' => $author['id'], 'title' => $author['title'],
                'description' => $author['description'], 'author' => $my_author['first_name'].' '.$my_author['last_name'],
                'tag' => $tag['tagName']
            ));
        }

        $data['status'] = true;
        echo json_encode($data);
    } catch (Exception $e) {
        // Error messages for debuging
        $tmp['status'] = false;
        $tmp['errorMessage'] = 'Failed to insert user into registry';
        $tmp['errorInfo'] = $sth->errorInfo();
        echo json_encode($tmp);
    }
} else if (isset($_GET['ownVids'])){
    
    // Checking if the user is loged in and are teacher/ admin
    if(isset($_SESSION['user']) && $_SESSION['accessLevel'] > 1){
        try {
            $sql = "SELECT * FROM video where author= ?";
            $sth = $db->prepare($sql);
            $sth->execute(array($_SESSION['user']));
            $tmp['my_vids'] = $sth->fetchAll(PDO::FETCH_ASSOC);
            
            //Getting all tags for the vids
            //Getting the authors name
            $data['data'] = [];
            foreach($tmp['my_vids'] as $author){
                $sql = "SELECT * FROM user where email= ?";
                $sth = $db->prepare($sql);
                $sth->execute(array($author['author']));
                $my_author= $sth->fetch(PDO::FETCH_ASSOC);

                $sql = "SELECT * FROM videotag where videoId= ?";
                $sth = $db->prepare($sql);
                $sth->execute(array($author['id']));
                $tag = $sth->fetch(PDO::FETCH_ASSOC);
                
                // putting it all together so i can use dome- repeat
                array_push($data['data'], array(
                    'id' => $author['id'], 'title' => $author['title'],
                    'description' => $author['description'], 'author' => $my_author['first_name'].' '.$my_author['last_name'],
                    'tag' => $tag['tagName']
                ));
            }

            $data['status'] = true;
            echo json_encode($data);
    } catch (Exception $e) {
        // Error messages for debuging
        $tmp['status'] = false;
        $tmp['errorMessage'] = 'Failed to insert user into registry';
        $tmp['errorInfo'] = $sth->errorInfo();
        echo json_encode($tmp);
    }
        
}
        
}