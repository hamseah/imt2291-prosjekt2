<?php
require_once '../Classes/Playlist.php';
require_once 'config.php';

session_start();
header("Access-Control-Allow-Origin: ".$config['AccessControlAllowOrigin']);
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");

//Checks if user is logged in before they are allowed to create playlist
if(isset($_SESSION)){
  $author = $_SESSION['user'];
  $title = $_POST['title'];
  $description = $_POST['description'];

  //Creates new playlist with the member function defined in the class
  $response = Playlist::createPlaylist($author, $title, $description);
  //Send the reponse back to the user, so they can see if the new playlist was sucessfully added to the database
  echo json_encode($response);
}
