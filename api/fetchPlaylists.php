<?php

require_once '../Classes/DB.php';
require_once '../Classes/Playlist.php';
require_once 'config.php';


session_start();
header("Access-Control-Allow-Origin: ".$config['AccessControlAllowOrigin']);
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");


//Checks if the person at least a student before fetching playlists
if($_SESSION['accessLevel']>0){
$author = $_SESSION['user'];

//Uses the member function to fetch all playlists belonging to a single user
$playlists = Playlist::getPlayList($author);

//Returns the playlists as json
echo json_encode($playlists);
}
