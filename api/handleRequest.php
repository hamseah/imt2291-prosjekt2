<?php

require_once '../Classes/DB.php';
require_once 'config.php';


session_start();
header("Access-Control-Allow-Origin: ".$config['AccessControlAllowOrigin']);
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");


//Checks if the person is a admin
if($_SESSION['accessLevel']==3) {
  $email = $_POST['email'];
  $action = $_POST['action'];

//Connects to DB
  $db = DB::getDBConnection();

//Checks if action != null, if true this mean the admin has accepted the request
//Then updates the users privilegies to become a teacher
if(isset($action)){
  $sql = "UPDATE user SET accessLevel = 2 WHERE email = '$email'";
  $sth = $db->prepare($sql);
  $sth->execute();
}
  //Regardless if the admin accepted/rejected the request, it must be removed from the datbase
  //Remove request from DB, regardless if the request was accepted/rejected
  $sql = "DELETE FROM request WHERE email = '$email'";
  $sth = $db->prepare($sql);
  $sth->execute();
}

exit();
