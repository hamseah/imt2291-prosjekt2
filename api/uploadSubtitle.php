<?php
require_once 'config.php';



header("Access-Control-Allow-Origin: ".$config['AccessControlAllowOrigin']);
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");


// PSUEDOCODE
//This endpoint is to upload the subtitles to the videos/subtitles folder.
// Use $SESSION to check that the user is logged in
//Use $_FILES[error] to check that no problem occured during upload
//Rename the file to reflect the videoID. Aka the file should be renamed videoID.vtt
//Use file_exists to check if the directory exists and also if a file with the same name exists to prevent duplicates
//Use move_uploaded_file to move the file to the directory if all goes well.
//Once the subtitles has been uploaded it should be available from the video viewing page
//Should return some response to the user wether it was sucessful or not.
