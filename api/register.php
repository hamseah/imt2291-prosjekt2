<?php

/**
 *  Skal være mellomledd mellom klienten og db-en. Får inn registreringsdata fra brukeren i form av POST
 *  Legger inn i db via classen User
 *  Deretter sender info tilbake, om status og navn på brukeren.
 * 
 **/
require_once 'config.php';
require_once '../Classes/user.php';

session_start();

header("Access-Control-Allow-Origin: ".$config['AccessControlAllowOrigin']);
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");

if(isset($_POST['fname'])){
    try{
        $data = array(
            'fname' => $_POST['fname'], 'lname' => $_POST['lname'], 'email' => $_POST['email'],
            'role' => $_POST['role'], 'password' => $_POST['password']
        );
        $user = new User();
        
        $res['feedback'] = $user->addUser($data);
        $res['userInfo'] = array(
            'fname' => $_POST['fname'], 'lname' => $_POST['lname'],
            'role' => $_POST['role']
        );
        echo json_encode($res);

    } catch (Exeption $e){
        $error['feedback'] = "Something went wrong";
        echo json_encode($error);
    }
    
}