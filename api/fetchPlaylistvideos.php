<?php

require_once '../Classes/DB.php';
require_once '../Classes/Playlist.php';
require_once 'config.php';


session_start();
header("Access-Control-Allow-Origin: ".$config['AccessControlAllowOrigin']);
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");


//Checks if the person at least a student before fetching videoes in specified playlist
if($_SESSION['accessLevel']>0){
  $author = $_SESSION['user'];
  $playlist = $_POST['id'];
  //Uses member function to fetch all playlists with a specific ID
  $playlist = Playlist::getVideosInPlaylist($playlist);
  //Returns the overview as json
  echo json_encode($playlist);
}
