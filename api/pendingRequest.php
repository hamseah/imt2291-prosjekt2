<?php


require_once 'config.php';
require_once '../Classes/admin.php';

session_start();


header("Access-Control-Allow-Origin: ".$config['AccessControlAllowOrigin']);
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");

$admin = new admin();
//Fetches pending requests from the database.
$res =  $admin->pendingRequests();
//And returns them as json
echo json_encode($res);
