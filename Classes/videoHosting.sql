DROP DATABASE IF EXISTS videoHosting;

CREATE DATABASE videoHosting COLLATE = utf8_danish_ci;

USE videoHosting;


CREATE TABLE Request(
		email VARCHAR(100) NOT NULL, 	# user that has request teacher accesslevel

		PRIMARY KEY (email)
);

CREATE TABLE User(
	email VARCHAR(100) NOT NULL, 	# Used as username
	accessLevel INT NOT NULL, 		# 1 student, 2 teacher, 3 admin
	password VARCHAR(255) NOT NULL,	# should be hashed and salted by php password_hash()
	first_name varchar(100) NOT NULL, # users firstname 
	last_name varchar(100) NOT NULL, # lastname of the user

	PRIMARY KEY (email)
);

CREATE TABLE Video(
	id INT NOT NULL AUTO_INCREMENT,
	title VARCHAR(50) NOT NULL,
	description VARCHAR(500) NOT NULL,
	author VARCHAR(100) NOT NULL,

	PRIMARY KEY (id),
	CONSTRAINT Video_author_User_email FOREIGN KEY (author) REFERENCES User (email)
);

CREATE TABLE Tag(	# Search vaariables
	name VARCHAR(50) NOT NULL,

	PRIMARY KEY (name)
);

CREATE TABLE Comment(
	id INT NOT NULL AUTO_INCREMENT,
	content VARCHAR(500) NOT NULL,

	author VARCHAR(100) NOT NULL,
	videoId INT NOT NULL,

	PRIMARY KEY (id),
	CONSTRAINT Comment_author_User_email FOREIGN KEY (author) REFERENCES User (email),
	CONSTRAINT Comment_videoId_Video_id FOREIGN KEY (videoId) REFERENCES Video (id)
);

CREATE TABLE PlayList(
	id INT NOT NULL AUTO_INCREMENT,
	title VARCHAR(50) NOT NULL,
	description VARCHAR(500) NOT NULL,

	author VARCHAR (100) NOT NULL,

	PRIMARY KEY (id, author),
	CONSTRAINT PlayList_author_User_email FOREIGN KEY (author) REFERENCES User(email)
);

CREATE TABLE VideoInPlayList(
	position INT NOT NULL,

	listId INT NOT NULL,
	videoId INT NOT NULL,


	CONSTRAINT unique_position_listId UNIQUE(listId, position),
	PRIMARY KEY (listId, videoId),
	CONSTRAINT VideoInPlayList_listId_PlayList_id FOREIGN KEY (listId) REFERENCES PlayList (id),
	CONSTRAINT VideoInPlayList_videoId_Video_id FOREIGN KEY (videoId) REFERENCES Video (id)
);

CREATE TABLE VideoTag(

	videoId INT NOT NULL,
	tagName VARCHAR (50) NOT NULL,

	PRIMARY KEY (videoId, tagName),
	CONSTRAINT VideoTag_videoId_Video_id FOREIGN KEY (videoId) REFERENCES Video (id),
	CONSTRAINT VideoTag_tagName_Tag_name FOREIGN KEY (tagName) REFERENCES Tag (name)
);

CREATE TABLE Rating(
	rating INT NOT NULL,	#0-5

	author VARCHAR(100) NOT NULL,
	videoId INT NOT NULL,

	PRIMARY KEY(author, videoId),
	CONSTRAINT Rating_author_User_email FOREIGN KEY (author) REFERENCES User (email),
	CONSTRAINT Rating_videoId_Video_id FOREIGN KEY (videoId) REFERENCES Video (id)
);
CREATE TABLE Subscription(

	user VARCHAR(100) NOT NULL,
	playListId INT NOT NULL,

	PRIMARY KEY (user, playListId),
	CONSTRAINT Subscription_user_User_email FOREIGN KEY (user) REFERENCES User (email),
	CONSTRAINT Subscription_playListId_PlayList_id FOREIGN KEY (playListId) REFERENCES PlayList (id)
);
