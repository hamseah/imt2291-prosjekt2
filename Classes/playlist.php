<?php

require_once 'DB.php';
/**
* Skal håndtere vid administrerering til og fra db
*/

class Playlist{
  private $db;

  /**
  * Contructoren kobler til db med PDO
  */
  public function __construct(){

  }

  /**
  * Stenger kobling til db når objektet dør
  */
  public function __destruct() {
    if ($this->db!=null) {
      unset ($this->db);
    }
  }

  /**
  * @param takes in 3 parameters, one for author, title and description
  * @return array return wether it was sucessfully added to the DB
  */
  public function createPlaylist($author, $title, $description) {
    $db = DB::getDBConnection();
    $sql = "INSERT INTO playlist(title, description, author)
    VALUES ('$title', '$description', '$author')";
    $sth = $db->prepare($sql);
    $sth->execute();
    if($sth->rowCount()==1){
      return "OK";
    }
    else {
      return "Failed to create playlist";
    }
  }


  /**
  * @param string $author is the email of the teacher that uploaded List.
  * @return array with videos in playlist, empty array if $author has not uploaded
  * or is not a teacher.
  */
  public function getPlayList($author){
    $db = DB::getDBConnection();

    $sql = "SELECT * FROM playlist
    WHERE playlist.author = '$author'";
    $sth = $db->prepare($sql);
    $sth->execute();
    $playlist= $sth->fetchAll(PDO::FETCH_ASSOC);
    return $playlist;
  }

  public function getVideosInPlaylist($playlistID){
    $db = DB::getDBConnection();

    $sql = "SELECT * FROM videoinplaylist
    INNER JOIN video
    ON videoinplaylist.videoId = video.id
    WHERE videoinplaylist.listId = '$playlistID'
    ORDER BY videoinplaylist.position";
    $sth = $db->prepare($sql);
    $sth->execute();
    $playlist= $sth->fetchAll(PDO::FETCH_ASSOC);
    return $playlist;
    }

    /**
    * Henter alle videoe til den studenten som er pålogget
    * @param string: id til studenten
    * @return array: med videoene studenten abonennerer på og status på operasjonen
    */
    public function getStudPlaylist($id){
    $tmp = [];

    try{
    $quary = 'select Favorit from students where id= ?';
    $smt = $this->db->prepare ($quary);
    $smt->execute(array($id));
    $emne = $smt->fetch(PDO::FETCH_ASSOC);
    $sql = 'Select * from videos where EmneCode = ?';
    $sth = $this->db->prepare ($sql);
    $sth->execute(array($emne['Favorit']));
    $tmp['vid'] = $sth->fetchAll(PDO::FETCH_ASSOC);
    $sql = 'Select rate, comments, videoId, studentId from videoinfo';
    $sth = $this->db->prepare ($sql);
    $sth->execute(array());
    $tmp['videoInfo'] = $sth->fetchAll(PDO::FETCH_ASSOC);
    $tmp['status'] = 'OK';
    } catch (Exception $e) {
    $tmp['status'] = 'FAIL';
    $tmp['errorMessage'] = 'Failed to get the playlist';
    $tmp['errorInfo'] = $e->getMessage();
    }

    return $tmp;
    }

    /**
    * Henter alle videoe til den studenten som er pålogget
    * @param string: id til lærer
    * @return array: med videoene studenten abonennerer på og status på operasjonen
    */
    public function teacherPlaylist($id){
    $tmp = [];

    try{
    $quary = 'select Subject from teachers where id= ?';
    $smt = $this->db->prepare ($quary);
    $smt->execute(array($id));
    $emne = $smt->fetch(PDO::FETCH_ASSOC);
    $sql = 'Select * from videos where EmneCode = ?';
    $sth = $this->db->prepare ($sql);
    $sth->execute(array($emne['Subject']));
    $tmp['vid'] = $sth->fetchAll(PDO::FETCH_ASSOC);
    $sql = 'Select rate, comments from videoinfo';
    $sth = $this->db->prepare ($sql);
    $sth->execute(array());
    $tmp['videoInfo'] = $sth->fetchAll(PDO::FETCH_ASSOC);
    $tmp['status'] = 'OK';
    } catch (Exception $e) {
    $tmp['status'] = 'FAIL';
    $tmp['errorMessage'] = 'Failed to get the playlist';
    $tmp['errorInfo'] = $e->getMessage();
    }

    return $tmp;
    }

    /**
    *  Sletter en video fra playlisten og databasen
    * @param string: id til vid som skal slettes
    * @return array: med info om status på operasjonen
    */
    public function deleteVid($id){
    $tmp = [];

    try{
    $sql = 'delete from videos where id = ?';
    $sth = $this->db->prepare ($sql);
    $sth->execute(array($id));
    $tmp['status'] = 'OK';
    } catch (Exception $e) {
    $tmp['status'] = 'FAIL';
    $tmp['errorMessage'] = 'Failed to get the playlist';
    $tmp['errorInfo'] = $e->getMessage();
    }

    return $tmp;
    }

    }
