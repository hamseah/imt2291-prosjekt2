<?php

/**
 * Class contaning functions for creating a new database and connecting to the database
 */
class DB {
  private static $db=null;
  private $dsn = 'mysql:host=localhost;dbname=videoHosting;charset=utf8';
  private $user = '';
  private $password = '';
  private $dbh = null;

  private function __construct() {

    // Getting sensitive info from config
    try {
      $raw = file_get_contents('../config/config.json');
      $json = json_decode ($raw, true);

      // Setting user and password
      $this->user=$json['DB']['user'];
      $this->password=$json['DB']['password'];

    }catch(Exception $e){
      // NOTE DO NOT USE THIS IN PRODUCTION
      echo 'Reading config failed: ' . $e->getMessage();
    }

    try {
        $this->dbh = new PDO($this->dsn, $this->user, $this->password);
    } catch (PDOException $e) {
        // NOTE DO NOT USE THIS IN PRODUCTION
        // Debug info
        echo 'Connection failed: ' . $e->getMessage();
    }
  }

  //Used to connect to the database from other functions
  public static function getDBConnection() {
      if (DB::$db==null) {
        DB::$db = new self();
      }
      return DB::$db->dbh;
  }
}
