<?php

/**
 *  The user class communicates with the db. Handles user- interactions e.g registrations,
 *  login, gets accesslevel (1 student, 2 teacher, 3 admin) and other interactions.
 */

 include_once 'DB.php';

class User{
    private $db;

    /**
     *  Gets connection to the db.
     */
    public function __construct(){
      $this->db = DB::getDBConnection();
    }

    /**
     * Closes the db- con, sets the local $db to null.
     */
    public function __destruct() {
        if ($this->db!=null) {
          unset ($this->db);
        }
    }


    /**
     * Handles the user registration
     * @param: array with the userdata thats to be inserted to db
     * @return: array with feedback info / success or fail and so on
    **/
    public function addUser($data){
        $tmp = [];
        
        try {
            $sql = 'insert into user (first_name, last_name, email, accessLevel, password) VALUES (?, ?, ?, ?, ?)';
            $sth = $this->db->prepare ($sql);
            $pass = password_hash($data['password'], PASSWORD_DEFAULT);
            $sth->execute (array ($data['fname'], $data['lname'],
            $data['email'], $data['role'], $pass));
            //Checks if the requestTeacher insertion went well, setting OK as default(incase of student registering).
            $tmp['req'] = 'OK';
            //Only if the accesslevel is higher then student.
            if($data['role'] >= 2){
                $res = $this->requestTeacher($data['email']);
                if($res['status'] == 'FAIL') $tmp['req'] = 'FAIL';
            }
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert user into registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
        
        if ($sth->rowCount()==1 && $tmp['req'] == 'OK') {
            $tmp['status'] = 'OK';
            $tmp['id'] = $this->db->lastInsertId();
        } else {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'New user could not be inserted into database';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
        return $tmp;
    }

    /**
     * Handles the user login, verifies if the email and password is correct
     * @param: array with login- data(epost, passord, accesslevel)
     * @return: array with feedback info / success or fail and so on
     */

    public function login($data){
        $tmp = [];

        try {
            $sql = 'SELECT email, password, accessLevel, first_name, last_name FROM user WHERE email = ?';
            $sth = $this->db->prepare ($sql);
            $sth->execute (array ($data['email']));
            $data_array = $sth->fetch(PDO::FETCH_ASSOC);
            if(password_verify($data['password'], $data_array['password'])){
                $tmp['status'] = 'OK';
                $tmp['role'] = $data_array['accessLevel'];
                $tmp['fname'] = $data_array['first_name'];
                $tmp['lname'] = $data_array['last_name'];
                $tmp['feedback'] = '0';             //feedback kode, if 0 = everything went well.
            } else{
                //for debuging
                $tmp['status'] = 'FAIL';
                $tmp['errorInfo'] = 'Wrong email or password..';
                $tmp['feedback'] = '1';             //feedback kode, if 1 = wrong email or pass.
            }

        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Something failed with the query';
            $tmp['errorInfo'] = $sth->errorInfo();
            $tmp['feedback'] = '1';             //feedback kode, if 1 = wrong email or pass.
        }

        //Checking if the user is in the request table 
        //(Here restores all the new user who yet have being granted accesslevel higher that 2)
        if($data_array['accessLevel'] > 1){
            $sql = 'SELECT * from request where email= ?';
            $sth = $this->db->prepare ($sql);
            $sth->execute (array ($data['email']));
            if($sth->rowCount() == 1) {
                $tmp['status'] = 'FAIL';
                $tmp['feedback'] = '2';             //feedback kode, if 2 letting the user know that he waits permition from admin.
                $tmp['errorMessage'] = 'Can not access yet';
            } 
        }
        return $tmp;
    }
 

     // Checks the accesslevel as it is in the database for the user with email eqvivalent to parameter $userName
    public function getAccessLevel($userName){
        $tmp = [];
        try {
            $sql = "SELECT accessLevel
                    FROM user
                    WHERE email=:email";

            $sth = $this->db->prepare($sql);
            $sth->bindValue(':email', $userName, PDO::PARAM_STR);
            $sth->execute();
            $permissions = $sth->fetch(PDO::FETCH_ASSOC);
            return $permissions['accessLevel'];
        } catch (Exception $e) {
            $tmp['accessLevel'] = 0;
            $tmp['errorInfo'] = $sth->errorInfo();
        }
           
    }

       //When new users are created with the checkbox teacher checked, this function adds a new request to the admins
    public function requestTeacher($userName){
        $temp = [];

        try {
            $sql = "INSERT INTO request(email) VALUES (:email)";
            $sth = $this->db->prepare($sql);
            $sth->bindValue(':email', $userName, PDO::PARAM_STR);
            $sth->execute();
        } catch (Exception $e) {
            $tmp['errorInfo'] = $sth->errorInfo();
        }
        
        //If a new row was inserted we consider the request as sucessful
        //Copied Øivinds code and adjusted for use case
        if ($sth->rowCount() == 1) {
            $tmp['status'] = 'OK';
            $tmp['id'] = $this->db->lastInsertId();
        } else {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to create new request';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
        return $tmp;
    }

};
