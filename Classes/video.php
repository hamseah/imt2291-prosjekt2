<?php

require_once 'DB.php';
/**
*   Video handling
*/
class Video
{
	public $id;
	public $title;
	public $description;
	public $author;

	/**
	 * @param int $id is the id of a video in the database
	 *
	 * constucts a Video object from data in the database. If it does not exist, empty object is constructed.
	 */
	public function __construct($id)
	{
		$db = DB::getDBConnection();

		$sql = 'SELECT *
				FROM Video
				WHERE id = :id';

		$sth = $db->prepare($sql);

		$sth->bindValue(':id', $_GET['videoId'], PDO::PARAM_INT);

		$sth->execute();

		$video = $sth->fetch(PDO::FETCH_ASSOC);

		$this->id = $video['id'];
		$this->title = $video['title'];
		$this->description =$video['description'];
		$this->author =	$video['author'];
	}

	/**
	 * @return assosiative array with all titles of Videos in the database.
	 */
	public function getAllTitles()
	{
		$db = DB::getDBConnection();

		return $db->query('SELECT title
					FROM Video');
	}

	/**
	* @param string $user email of the teacher.
	* @return array with assosiative array with 'id' and 'title', Where id and title are id, and title of video,
	* Returns empty array if nothing was found
	*/
	public function getAllFromTeacher($user)
	{
		$db = DB::getDBConnection();
		$sql = 'SELECT id, title
				FROM Video
				WHERE Video.author = :author';

		$sth = $db->prepare($sql);
		$sth->bindValue(':author', $user, PDO::PARAM_STR);
		$sth->execute();
		return $sth->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 * @param integer $id is the video id.
	 * @return string with email of author.
	 */
	public function getAuthor($id){
		$db = DB::getDBConnection();

		$sql = 'SELECT author
				FROM Video
				WHERE id = :id';
		$sth = $db->prepare($sql);
		$sth->bindValue(':id', $id, PDO::PARAM_INT);
		$sth->execute();
		$author = $sth->fetch(PDO::FETCH_ASSOC);

		return $author['author'];
	}
}
